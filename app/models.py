from django.db import models

class User(models.Model):
	fullname = models.CharField(max_length=200)
	email =  models.CharField(max_length=200)
	active =  models.BooleanField(default=True)
	contact= models.IntegerField()
	image =  models.ImageField(upload_to='media/', default='insta-6.jpg')
	address = models.CharField(max_length=200)
	password=models.CharField(max_length=20,default='')

	def __str__(self):
		return self.fullname+" - "+str(self.email)+" - "+str(self.id)