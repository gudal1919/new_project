from django.http import Http404,HttpResponse
from django.shortcuts import get_object_or_404, render,redirect
from django.utils import timezone
from django import forms
from .models import User
import os
import json
from django.conf import settings
from django.http import JsonResponse

from rest_framework.response import Response

from django.views.decorators.csrf import csrf_protect
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser


def direct(request):
    context={}
    return render(request, 'direct.html', context)

@api_view(["POST", "GET"])
@parser_classes((JSONParser,))
def sign_in(request, format=None):
    print(request.method)
    if request.method == "GET":
        template = loader.get_template("sign_in.html")
        context = {}
        return HttpResponse(template.render(context, request))
    elif request.method == "POST":
        try:
            d=request.data
            print("d=",d)

            email=d['email']
            print("email",email)
            password=d['password']
            print("password",password)
            user = User.objects.get(email=email,password=password)
            print("user",user)
            return Response({"status": 200, "userId":user.id,"message": "Dear " + user.fullname + ", we successfully mailed your password"}, status=200)
        except :
            return Response({"status": 400, "message": "Email does not exist in our PMT account"}, status=200)

# @api_view(["POST", "GET"])
# def register(request):
#     context={}
#     print("get register data")
#     if request.method == "POST":
#         print("post register data")
#         print("request method",request.method)
#         print("request",request.POST)
#         # fullname=request.POST.__getitem__('fullname')
#         # address=request.POST.__getitem__('address')
#         # email=request.POST.__getitem__('email')
#         # contact=request.POST.__getitem__('contact')
#         # password=request.POST.__getitem__('password')
#         # # Updated.objects.all().update(fullname=j,address=j1,email=j2,contact=j3,password=j4)
#         # user=User(fullname=fullname,address=address,email=email,contact=contact,password=password)
#         # user.save()
#         # print("done")
#         # return Response({"status": 200, "message": "successfully registered", 'userId': user.id}, status=200)
#         for key in request.data:
#             data = key
#             break


#         print("data: ", data)
#         data = json.loads(data)
#         print(type(data))
#         fullname=data["fullname"]
#         email=data["email"]
#         contact=data["contact"]
#         address=data["address"]
#         password=data["password"]

#         user=User(fullname=fullname,address=address,email=email,contact=contact,password=password) 
#         user.save()
#         print("user saved")
#         return Response({"status": 200, "message": "successfully registered", 'userId': user.id}, status=200)
#     return render(request, 'sign_up.html', context)

@api_view(["POST", "GET"])
@parser_classes((JSONParser,))    
def sign_up(request, format=None):
    context={}
    print('register...')
    print(request.META)
    print(request.path,request.method)
    if request.method=="POST":
        print("null..............",request.POST)
        print("request data",request.data)
        # fullname=request.POST.__getitem__('fullname')// parser classes not using then get data.
        # email=request.POST.__getitem__('email')
        # contact=request.POST.__getitem__('contact')
        # address=request.POST.__getitem__('address')
        # password=request.POST.__getitem__('password')
        # for key in request.data:
        #     data = key
        #     break
        fullname=request.data['fullname']#parser classes using 
        email=request.data['email']
        contact=request.data['contact']
        address=request.data['address']
        password=request.data['password']

        # print("data: ", data)
        # data = json.loads(data)
        # print(type(data))
        # fullname=data["fullname"]
        # email=data["email"]
        # contact=data["contact"]
        # address=data["address"]
        # password=data["password"]

        user=User(fullname=fullname,address=address,email=email,contact=contact,password=password) 
        user.save()
        print("user saved")
        # try:
        #     email=email
        #     password=password
        #     user = User.objects.get(email=email,password=password)
        #     print("user",user)
        #     return Response({"status":200, "userId": user.id, message: "Dear"+user.fullname+" we successfully mailed your password"})
        # except :
        #     print('error')
        return Response({"status": 200, "message": "successfully registered", 'userId': user.id}, status=200)
 
    return render(request, 'sign_up.html', context)    

def profile(request,id):
    context={}
    print('userid',id)
    user = User.objects.get(pk=id)
    context['user']=user
    return render(request, 'profile.html', context)

@api_view(["POST", "GET"])
@parser_classes((JSONParser,))
def update(request,id,format=None):
    id=int(id)
    if request.method == "GET":
        user = User.objects.get(pk=id)
        context={'user':user}
        return render(request, 'update.html', context)

    elif request.method == "POST":

        fullname=request.data['fullname']#parser classes using 
        email=request.data['email']
        contact=request.data['contact']
        address=request.data['address']
        password=request.data['password']
       
        User.objects.filter(pk=id).update(fullname=fullname,address=address,email=email,contact=contact,password=password)

        return Response({"status": 200, "message": "successfully registered", 'userId': id}, status=200)
        # return redirect('/profile/'+id+'/')


# def updated(request):
#     id =request.POST.__getitem__('userId')
#     id=int(id)
#     print("id",id)
#     user = User.objects.get(pk=id)
#     fullname=request.POST.__getitem__('fullname')
#     address=request.POST.__getitem__('address')
#     email=request.POST.__getitem__('email')
#     contact=request.POST.__getitem__('contact')
#     password=request.POST.__getitem__('password')
    
#     User.objects.filter(pk=id).update(fullname=fullname,address=address,email=email,contact=contact,password=password)

#     return redirect('/profile/')

def delete(request,id):
    context={}
    id=int(id)
    user = User.objects.get(pk=id)
    user.active=False
    user.save()
    return redirect('/signin/')
